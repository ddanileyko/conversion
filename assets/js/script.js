$(document).ready(function() {

    $('#convert').on('click', function(e) {
        e.preventDefault();
        err_block = document.getElementById('conv_err');
        err_block.innerHTML = '';
        var valuta_2 = $('#valuta-2').val();
        var valuta_1 = $('#valuta-1').val();
        var to_conv = $('#to_conv').val();
        $.ajax({
              url: '/api/conversion/',
//              url: '/convert/',
              data: {'valuta_1': valuta_1, 'valuta_2': valuta_2, 'to_conv': to_conv},
              type: 'get',
              success: function(data) {
                if(data.success) {
                    err_block = document.getElementById('conv_res');
                    err_block.innerHTML = data.message
                } else {
                    err_block = document.getElementById('conv_err');
                    err_block.innerHTML = data.message
                }
              }
        });
    });

});
