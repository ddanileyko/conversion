from django.urls import path
from .views import ConversionView

app_name = "articles"

urlpatterns = [
    path('conversion/', ConversionView.as_view()),
]