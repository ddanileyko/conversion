from rest_framework.views import APIView
from rest_framework.response import	Response

from apps.helpers import conversion


class ConversionView(APIView):
    def get(self, request):
        return Response(conversion(request.GET.get('valuta_1', None), request.GET.get('valuta_2', None),
                                   request.GET.get('to_conv', None)))
