from django.db import models
from django.utils.translation import ugettext_lazy as _


class Valuta(models.Model):
    name = models.CharField(_('name'), max_length=3, db_index=True)
    rate = models.FloatField(_('rate'))
    added = models.DateTimeField(_('added'), db_index=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Rate')
        verbose_name_plural = _('Rate')

    def __str__(self):
        return u'%s' % self.name
