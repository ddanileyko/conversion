from django.contrib import admin


from .models import Valuta

class ValutaAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'rate', 'added',
    )

admin.site.register(Valuta, ValutaAdmin)    
