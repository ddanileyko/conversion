from django.urls import path

from .views import *

currency_urlpatterns = [
    path('', main_page, name='main_page'),
    path('convert/', convert, name='convert'),
]