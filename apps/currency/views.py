from django.shortcuts import render
from django.conf import settings
from jsonview.decorators import json_view

from apps.helpers import conversion


def main_page(request):
    template = 'currency/conversion.html'
    context = {'valuta': settings.VALUTA}
    return render(request, template, context)


@json_view
def convert(request):
    if request.method == 'GET':
        return conversion(request.GET.get('valuta_1', None), request.GET.get('valuta_2', None),
                          request.GET.get('to_conv', None))
