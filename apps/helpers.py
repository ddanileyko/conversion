import requests
import json
import datetime

from django.utils.translation import ugettext as _
from django.conf import settings

from apps.currency.models import Valuta


def conversion(valuta_1, valuta_2, to_conv):
    try:
        res = float(to_conv) / get_rate(valuta_1) * get_rate(valuta_2)
    except:
        return {'success': False, 'message': _('Error')}

    return {'success': True, 'message': "%.2f" % (res)}


def get_rate(valuta_name):
    valuta = Valuta.objects.filter(name=valuta_name).first()
    if not valuta or valuta.added.date() < datetime.datetime.now().date():
         r = requests.get(url='https://openexchangerates.org/api/latest.json?app_id=2c3a9fbc780648ff99d18972b0feb45c')
         rate = json.loads(r.text)['rates'][valuta_name]
         for val_name in settings.VALUTA:
             val_rate = json.loads(r.text)['rates'][val_name]
             val, created  = Valuta.objects.get_or_create(name=val_name, defaults={'rate': val_rate})
             if not created:
                 val.rate = val_rate
                 val.save()
         return rate
    else:
        return valuta.rate
